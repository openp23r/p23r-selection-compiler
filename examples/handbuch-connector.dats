	model Applicant = http://leitstelle.p23r.de/NS/P23R/ruleGroups/Starter/enterpriseCore1-0

	[

Die beiden letzten Adressen werden aus dem Unternehmsprofil gefiltert und kopiert.

		for applicant in Applicant.enterpriseCore.profile
		take 1
		return profile {
			@xmlns { "http://leitstelle.p23r.de/NS/P23R/ruleGroups/Starter/AddressChange/Selection1-0" }
			company { @name { applicant.company@name } }
			[
				foreach location in applicant.headquarters
				orderby location@validFrom
				take -2

				return addresses {
					@validFrom { location@validFrom }
					@lastModified { location@lastModified }
					@houseNumber { location.address@houseNumber }
					@street { location.address@street }
					@city { location.address@city }
					@zipCode { location.address@zipCode }
					@country { location.address@country }
				}
			]
		}
	]
