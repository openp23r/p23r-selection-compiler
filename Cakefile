#
# Cakefile to build node application
#
# The template supports standard directories as 'js', 'css', 'images' with a flat
# structure as well as individual subdirectories by inserting the standard directory
# name before the subdirectories to take over.
#
# The template compiles all files to 'bin' and afterwards copy the files into the
# final structure inside 'dist'.
#
# The templates distribute the follwing file types from the source:
#
# '.coffee', '.litcoffee', '.js', 'json', '.map', '.less', '.css',
# '.jpeg', '.png', '.ico', '.svg', 'jade', '.html', '.md', '.pdf', 'epub',
# '.zip' and 'Dockerfile'
#
# author: Jan Gottschick
#

spawn = require('child_process').spawn
flour = require 'flour'
fs = require 'fs'
gaze = require 'gaze'
glob = require 'glob'
mkdirp = require 'mkdirp'
path = require 'path'
PEG = require 'pegjs'
pegcoffee = require 'pegjs-coffee-plugin'

###########################################
#
# extend flour by jade compiler
#

flour.compilers['jade'] = (file, cb) ->
  jade = require 'jade'
  fn = file.name.replace(/^src\//, 'bin/').replace(/.jade$/, '.html')
  file.read (code) ->
    saveDir = process.cwd()
    process.chdir file.dir
    try
      result = jade.render code, { filename: fn, pretty: true }
    catch error
      console.log error.message
    finally
      process.chdir saveDir
      cb result or ''

###########################################
#
# basic tasks for compiling, copying and deploying
#

task 'build:jade', 'compiling index.jade files', ->
  sources = glob.sync 'src/**/index.jade'
  for source in sources
    do (source) ->
      target = source.replace(/^src\//, 'bin/').replace(/.jade$/, '.html')
      compile source, target, ->
        copyToDist target

task 'build:coffee', 'compiling coffeescript files', ->
  buildCoffee 'src/**/*.+(coffee|litcoffee)'

task 'build:js', 'copying javascript files', ->
  copyToBinDist 'src/**/*.+(js|map)', 'javascript', 'js'

task 'build:less', 'compiling less files', ->
  sources = glob.sync 'src/**/*.less'
  for source in sources
    do (source) ->
      target = source.replace(/^src\//, 'bin/').replace(/.less$/, '.css')
      compile source, target, ->
        copyToDist target, 'css'

task 'build:css', 'copying css files', ->
  copyToBinDist 'src/**/*.css', 'stylesheets', 'css'

task 'build:images', 'copying image files', ->
  copyToBinDist 'src/**/*.+(jpeg|png|ico|svg)', 'images', 'images'

task 'build:doc', 'copying documenation files', ->
  copyToBinDist 'src/**/*.md', 'docs', 'doc'

task 'build:pegcoffee', 'compiling pegcoffee files', ->
  buildPegCoffee 'src/**/*.pegcoffee'

task 'build:others', 'copying other files', ->
  copyToBinDist 'src/**/+(*.+(pdf|epub|zip|html|ics|json)|Dockerfile)', 'other'

task 'build:bower', 'install packages from bower.io', ->
  spawn 'bower', ['install'], {stdio: "inherit"}
  console.log "Installed bower packages"

task 'deploy:cli', 'copying command line files to deployment directory', ->
  deployFiles 'cli'

task 'deploy:lib', 'copying library files to deployment directory', ->
  deployFiles 'lib'

task 'deploy:packageJson', 'copying package.json file to deployment directory', ->
  try
    packageJson = JSON.parse(fs.readFileSync('package.json'))
    targetPackageJson =
      name: packageJson.name
      description: packageJson.description
      version: packageJson.version
      dependencies: packageJson.dependencies
      engines: packageJson.engines
      repository: packageJson.repository
      author: packageJson.author
      license: packageJson.license
    fs.writeFileSync(packageJson.deploymentDirectory + '/package.json', JSON.stringify(targetPackageJson, null, 2))
    copyFile('dats2xquery', packageJson.deploymentDirectory + '/dats2xquery')
    copyFile('README.md', packageJson.deploymentDirectory + '/README.md')
    copyFile('LICENSE', packageJson.deploymentDirectory + '/LICENSE')
    console.log "Deployed package.json, LICENSE and README.md"
  catch error
    console.log error.message

task 'test:jasmine', 'test spec files', ->
  sources = glob.sync 'dist/**/*Spec.js'
  spawn 'jasmine-node', ['--autotest', '--verbose'].concat(sources), {stdio: "inherit"}

###########################################
#
# utilities
#

removeDir = (path) ->
  spawn 'rm', ['-rf', path], {stdio: "inherit"}
  console.log "Removed directory " + path + "/**/*"

# relocate files from <level1>/<level2>/**/<dir>/<**>/x.y
#                  to <level1>/<level2>/<dir>/<**>/x.y
# relocate files from <level1>/<level2>/**/x.y
#                  to <level1>/<level2>/<dir>/x.y
relocate = (s, dir) ->
  matcher1 = '^(\\w+\\/\\w+\\/)((\\w+\\/)*)' + dir + '\\/((\\w+\\/)*)([^/]+)$'
  matcher2 = '^(\\w+\\/\\w+\\/)((\\w+\\/)*)([^/]+)$'
  m1 = s.match(matcher1)
  m2 = s.match(matcher2)
  if m1
    return m1[1] + dir + "/" + m1[4] + m1[6]
  else if m2
    return m2[1] + dir + "/" + m2[4]
  else
    return s

copyToBinDist = (files, name, dir) ->
  sources = glob.sync(files)
  for source in sources
    do (source) ->
      if not source.match(/^[\w\/]+Test.\w+$/)
        target = source.replace(/^src\//, 'bin/')
        copyFile source, target
        copyToDist target, dir
        console.log "Copied " + name + " " + source

copyToDist = (source, dir) ->
  target = source.replace(/bin\//, 'dist/')
  target = relocate target, dir if dir
  if source.match /.html$/
    try
      # modify html files to insert files, which are loaded by bower
      fs.createReadStream(source)
        .pipe(fs.createWriteStream(target))
    catch error
      console.log error.message
  else
    copyFile source, target

copyFile = (source, target) ->
  targetDir = target.split('/')[...-1].join('/')
  mkdirp.sync targetDir
  fs.writeFileSync target, fs.readFileSync(source)

deployFiles = (dir) ->
  try
    packageJson = JSON.parse(fs.readFileSync('package.json'))
    sources = glob.sync('dist/' + dir + '/**/*')
    for source in sources
      if fs.statSync(source).isFile()
        do (source) ->
          target = source.replace(RegExp('^dist\\/'), packageJson.deploymentDirectory + '/')
          copyFile source, target
          console.log "Deployed " + target
  catch error
    console.log error.message

###########################################
#
# watcher utilities
#

watcher = (files, f) ->
  console.log 'Watching ' + files
  gaze files, (err, watcher) ->
    @on 'changed', (filepath) ->
        f path.relative(process.cwd(), filepath)
    @on 'added', (filepath) ->
        f path.relative(process.cwd(), filepath)

buildCoffee = (sources, cb) ->
  for source in glob.sync(sources)
    do (source) ->
      if not source.match(/^[\w\/]+Test.\w+$/)
        target = source.replace(/src\//, 'bin/').replace(/.litcoffee$/, '.js').replace(/.coffee$/, '.js')
        targetDir = target.split('/')[...-1].join('/')
        mkdirp.sync targetDir
        coffee = spawn 'coffee', ['-c', '-o', targetDir, source], { stdio: 'inherit' }
        coffee.on "uncaughtException", (error) ->
          console.log error
        coffee.on "close", (code) ->
          console.log "Compiled " + source
          copyToDist target, 'js'
          cb(target) if cb

buildPegCoffee = (sources, cb) ->
  for source in glob.sync(sources)
    do (source) ->
      target = source.replace(/src\//, 'bin/').replace(/.pegcoffee$/, '.js')
      targetLib = source.replace(/src\//, 'bin/').replace(/.pegcoffee$/, 'Lib.js')
      targetDir = target.split('/')[...-1].join('/')
      exportVar = target.split('/')[-1...][0].replace(/.js$/, '')
      mkdirp.sync targetDir
      fs.writeFileSync target, "module.exports = " + PEG.buildParser(
        fs.readFileSync(source, {encoding:'utf8'}),
        {
          plugins: [pegcoffee], output: 'source'
        }
      )
      console.log "Compiled " + source
      copyToDist target, 'js'
      fs.writeFileSync targetLib, exportVar + " = " + PEG.buildParser(
        fs.readFileSync(source, {encoding:'utf8'}),
        {
          plugins: [pegcoffee], output: 'source'
        }
      )
      console.log "Compiled library " + source
      copyToDist targetLib, 'js'

buildJs = (source) ->
  copyToBinDist(source, 'javascript', 'js')

###########################################
#
# The available main tasks to run
#

task 'clean', 'use this to remove all files from bin and dist', ->
  removeDir('bin')
  removeDir('dist')

task 'build', 'use this to build the application from ground up', ->
  invoke 'build:jade'
  invoke 'build:coffee'
  invoke 'build:js'
  invoke 'build:less'
  invoke 'build:css'
  invoke 'build:images'
  invoke 'build:pegcoffee'
  invoke 'build:doc'
  invoke 'build:others'
  invoke 'build:bower'

task 'watch', 'use this to watch changes an update the application', ->
  watch 'src/**/*.jade', -> invoke 'build:jade'
  watcher 'src/**/*.+(coffee|litcoffee)', buildCoffee
  watcher 'src/**/*.+(js|map)', buildJs
  watch 'src/**/*.less', -> invoke 'build:less'
  watch 'src/**/*.css', -> invoke 'build:css'
  watch 'src/**/*.md', -> invoke 'build:doc'
  watch 'src/**/*.+(jpeg|png|ico|svg)', -> invoke 'build:images'
  watcher 'src/**/*.pegcoffee', buildPegCoffee
  watch 'src/**/+(*.+(md|pdf|epub|zip|html|ics)|Dockerfile)', -> invoke 'build:others'

task 'serve', 'use this to start the application server', ->
  spawn 'node', ['dist/server/js/server.js'], {stdio: "inherit"}

task 'deploy', 'use this to deploy the application', ->
  invoke 'deploy:cli'
  invoke 'deploy:lib'
  invoke 'deploy:packageJson'

task 'test', 'use this to execute all tests for the application', ->
  invoke 'test:jasmine'
