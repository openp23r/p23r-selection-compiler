# README.md

Author: Jan Gottschick <service@openp23r.org>

This is a P23R Selection language to Xquery code compiler. P23R Selection is part
of the P23R project and described in its specifications, which are available at

http://www.p23r.de or
https://entwickler.p23r.de


## Usage as a command

The compiler is implemented in Javascript and requires *node.js*, which is available
at

http://nodejs.org

After installing node.js you can install the p23r selection compiler, globally, by

	npm install p23r-selection-compiler -g

and start the compiler by

	dats2xquery

You can enter the p23r selection source code via stdin and the target code will
appear on stdout, e.g. code for the p23r processor

	echo "I'm a comment" | dats2xquery processor

will output

	(: I'm a comment :)

The following options are available:

	Usage:
	  dats2xquery [OPTIONS] [infile [outfile]]

	Create Xquery code from P23R selection expressions.

	Options:
		-s, --start [STRING]   specify a different start rule (Default is start)
		-k, --no-color         Omit color from output
		--debug                Show debug information
		-v, --version          Display the current version
		-h, --help             Display help and usage details

	Commands:
	connector, processor

You must include a command as part of the options to specify for which
p23r component the code should be generated.

## Usage inside your code

You can include the p23r selection compiler in your project, too. If you
build a server side service using node.js just include `p23r-selection-compiler`
in your `package.json` file. You can call the command using a relative path
to `node_modules/p23r-selection-compiler/dats2xquery`. Or you use the library
the usual way, e.g. in coffeescript

	compiler = require 'p23r-selection-compiler'

	...
	connectorTargetCode = compiler.xqueryForConnector(connectorSourceCode)
	processorTargetCode = compiler.xqueryForProcessor(processorSourceCode)

If you like to use the compiler inside your browser code you can load
the p23r selection compiler by using bower. Just include `p23r-selection-compiler`
in your `bower.json` file. Then load the files

* ../p23r-selection-compiler/dist/lib/js/selectionConnectorLib.js
* ../p23r-selection-compiler/dist/lib/js/selectionProcessorLib.js

with your web page and call the parser

	connectorTargetCode = selectionConnector.parse(connectorSourceCode)
	processorTargetCode = selectionProcessor.parse(processorSourceCode)

Errors can be handled in both cases using try and catch, e.g.

	try
		connectorTargetCode = selectionConnector.parse(connectorSourceCode)
	catch error
		console.log error.name + " in selection at " + error.line + "," + error.column + ": " + error.message

## Legal issues

This project is part of the *openP23R* initiative. All related projects
are listed at https://gitlab.com/openp23r/openp23r .

The legal conditions see *LICENSE* file.

The project is maintained by P23R-Team (a) Fraunhofer FOKUS