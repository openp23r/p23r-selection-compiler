// Generated by CoffeeScript 1.9.3
(function() {
  var execute;

  require('jasmine-matchers');

  require('jasmine-given');

  execute = require('./basexUtil').execute;

  xdescribe('the basex XQuery service', function() {
    it('should be available', function(done) {
      return execute(done, 'info', function(err, reply) {
        return expect(reply.ok).toBe(true);
      });
    });
    it('should run a correct script', function(done) {
      return execute(done, 'xquery 1 to 5', function(err, reply) {
        return expect(reply.result).toBe('1 2 3 4 5');
      });
    });
    return it('should signal an incorrect script', function(done) {
      return execute(done, 'xry 1 to 5', function(err, reply) {
        return expect(reply.ok).toBe(false);
      });
    });
  });

}).call(this);
