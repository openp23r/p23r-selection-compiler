### Start Parser to generate Xquery code

Author goa

	SelectionParserXquery = exports? and exports or @SelectionParserXquery = {}

	SelectionProcessor = require('../../lib/js/selectionProcessor')
	SelectionConnector = require('../../lib/js/selectionConnector')

Generate Xquery code for the P23R Processor as output

	SelectionParserXquery.processorCode = (input, options, logger) ->
		try
			logger.debug "Start parsing for processor..."
			out = SelectionProcessor.parse(input)
		catch error
			logger.fatal error.name + " in SelectionParserXquery at " + error.line + "," + error.column + ": " + error.message
		return out

Generate Xquery code for a Source Connector as output

	SelectionParserXquery.connectorCode = (input, options, logger) ->
		try
			logger.debug "Start parsing selection for connector ..."
			out = SelectionConnector.parse(input)
		catch error
			logger.fatal error.name + " in SelectionParserXquery at " + error.line + "," + error.column + ": " + error.message
		return out
