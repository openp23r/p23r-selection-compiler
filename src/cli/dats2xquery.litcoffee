### Command Line Interface for P23R selection compiler

Author Jan Gottschick

#### Imports

Beside the standard libraries to parse a cli and accessing files the parser and its contained
generators for documentation in markdown and literate view as well for the xquery code are
required.

	fs = require('fs')
	cli = require 'cli'

	try
		SelectionParserXquery = require('../../lib/js/selectionParserXquery')
	catch error
		cli.fatal error.name + " importing SelectionParserXquery " + error.message

#### command line options

The library cli is used to parse the command line options. The library is also used to show all error
messages and information to the user.

	version = 0.1

	cli.enable('version','status')
	   .setApp('dats2xquery', version)
	   .setUsage("dats2xquery [OPTIONS] [infile [outfile]]\n\n" +
			"Create Xquery code from P23R selection expressions.")

	cli.argv.shift() if cli.argv[0]?.indexOf("dats2xquery") > 0

	options = cli.parse({
		start:   ['s', 'specify a different start rule', 'string', 'start']
	}, ['processor', 'connector'])

The tool selection translates P23R selection source files into Xquery code.
selection supports two arguments.

The argument inFile is the input file to parse.
The argument outFile is optional and is the output file which contains the result. If no output file
is given the result will be printed on the console.

Read and check the given arguments at the command line.

	cli.debug "fs: " + JSON.stringify(fs)

	inFile = if cli.args.length then cli.args.shift() else ''
	outFile = if cli.args.length then cli.args.shift() else ''

Get the standard input stream or an input file stream

	cli.debug "infile: " + inFile
	cli.debug "outFile: " + outFile
	try
		s = if inFile then fs.createReadStream(inFile, {encoding:'utf8'}) else process.stdin
	catch error
		cli.error "Cannot load inFile: #{inFile}"
		cli.fatal error

Read the input stream

	data = ''
	s.on 'data', (chunk)->
			data += chunk

Create the Xquery code.

	s.on 'end', ->
		try
			cli.debug "create xquery code..."
			if cli.command == 'connector'
				output = SelectionParserXquery.connectorCode(data, options, cli)
			else
				output = SelectionParserXquery.processorCode(data, options, cli)
			cli.debug "get resulting xquery code\n" + output
		catch error
			cli.fatal error.name + " in dats2xquery at " + error.line + "," + error.column + ": " + error.message

Write the result back to a given file or the console.

		cli.debug "writing result"
		if outFile
			try
				fs.writeFileSync(outFile, output, 'utf8')
			catch error
				cli.fatal  "Cannot write outFile: #{outFile}"
		else
			console.log output
