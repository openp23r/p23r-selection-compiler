
## P23R Selection Compiler - basic selection tests with selectors

Author: Jan Gottschick

To test the P23R selection compiler for the connector ...

Importing the Jasmine test framework addons to describe the specifications by
examples.

		require 'jasmine-matchers'
		require 'jasmine-given'

		specUtilConnector = require './specUtilConnector'
		compile = specUtilConnector.compile
		compileQuery = specUtilConnector.compileQuery
		query = require('./basexUtil').query
		setup = require('./basexUtil').setup

And the tests...

		describe 'P23R Selection @ connector for absolute selectors', ->

			beforeEach (done) ->
				setup ->
					done()
				, 'selectorProfile', '''
					<profile>
						<b c="0815"/>
						<d>xyz</d>
						abc
					</profile>
				'''

			it 'should accept one model definition statements', (done) ->
				compile done, '''
				 
					model Xyz=http://www.pp.de/NS/22/hs
					result {
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain 'namespace Xyz = "http://www.pp.de/NS/22/hs";'

			it 'should accept multiple model definition statements', (done) ->
				compile done, '''
				 
					model Xyz=http://www.pp.de/NS/22/hs
					model Uvw=http://www.pp.de/NS/23/hs

					result {
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain 'namespace Xyz = "http://www.pp.de/NS/22/hs";'
					expect(result).toContain 'namespace Uvw = "http://www.pp.de/NS/23/hs";'

			it 'should accept accept absolute selectors outside a query statement', (done) ->
				compile done, '''
				 
					model Xyz=http://www.pp.de/NS/22/hs
					result {
						Xyz
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '{ / }'

			it 'should accept accept absolute selectors with a path outside a query statement', (done) ->
				compile done, '''
				 
					model Xyz=http://www.pp.de/NS/22/hs
					result {
						Xyz.a
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '{ /*:a }'

			it 'should accept accept absolute selectors with a path and an attribute outside a query statement', (done) ->
				compile done, '''
				 
					model Xyz=http://www.pp.de/NS/22/hs
					result {
						Xyz.a@b
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '{ fn:data(/*:a/@*:b) }'

		describe 'P23R Selection @ connector for variables', ->

			it 'should reject undefined variables', (done) ->
				compile done, '''
				 
					result {
						ufo
					}
				''', (ok, result) ->
					expect(ok).toBe false
