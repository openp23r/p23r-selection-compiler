
## P23R Selection Compiler - basic selection tests with selectors

Author: Jan Gottschick

To test the P23R selection compiler for the processor ...

Importing the Jasmine test framework addons to describe the specifications by
examples.

		require 'jasmine-matchers'
		require 'jasmine-given'

		specUtilProcessor = require './specUtilProcessor'
		compile = specUtilProcessor.compile
		compileQuery = specUtilProcessor.compileQuery
		query = require('./basexUtil').query
		setup = require('./basexUtil').setup

And the tests...

		describe 'P23R Selection @ processor for absolute selectors', ->

			beforeEach (done) ->
				setup ->
					done()
				, 'selectorSource', '''
					<a>
						<b c="0815"/>
						<d>xyz</d>
						abc
					</a>
				'''
				setup ->
					done()
				, 'selectorProfile', '''
					<profile/>
				'''

			it 'should accept one model definition statements', (done) ->
				compile done, '''
				 
					model Xyz=http://www.pp.de/NS/22/hs
					result {
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain 'declare variable $__ns_Xyz := "http://www.pp.de/NS/22/hs";'

			it 'should accept multiple model definition statements', (done) ->
				compile done, '''
				 
					model Xyz=http://www.pp.de/NS/22/hs
					model Uvw=http://www.pp.de/NS/23/hs

					result {
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain 'declare variable $__ns_Xyz := "http://www.pp.de/NS/22/hs";'
					expect(result).toContain 'declare variable $__ns_Uvw := "http://www.pp.de/NS/23/hs";'

			it 'should accept absolute selectors outside a query statement', (done) ->
				compile done, '''
				 
					model Xyz=http://www.pp.de/NS/22/hs
					result {
						Xyz
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '{ p23r:selectSourceData( $__ns_Xyz, "\tmodel Xyz=http://www.pp.de/NS/22/hs\n\tXyz" ) }'

			it 'should accept absolute selectors with a path outside a query statement', (done) ->
				compile done, '''
				 
					model Xyz=http://www.pp.de/NS/22/hs
					result {
						Xyz.a
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '{ p23r:selectSourceData( $__ns_Xyz, "\tmodel Xyz=http://www.pp.de/NS/22/hs\n\tXyz.a" ) }'

			it 'should accept absolute selectors with a path and an attribute outside a query statement', (done) ->
				compile done, '''
				 
					model Xyz=http://www.pp.de/NS/22/hs
					result {
						Xyz.a@b
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '{ p23r:selectSourceData( $__ns_Xyz, "\tmodel Xyz=http://www.pp.de/NS/22/hs\n\tXyz.a@b" ) }'

		describe 'P23R Selection @ processor for source selectors', ->

			it 'should accept one source definition statement', (done) ->
				compile done, '''
				 
					source http://www.pp.de/NS/rule0815/source
					result {
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain 'declare variable $__ns__source := "http://www.pp.de/NS/rule0815/source";'
					expect(result).toContain 'declare variable $__source := doc($__ns__source);'

			it 'should reject multiple source definition statements', (done) ->
				compile done, '''
				 
					source http://processor.p23r.de/rule0815/source
					source http://processor.p23r.de/rule007/source

					result {
					}
				''', (ok, result) ->
					expect(ok).toBe false

			it 'should accept source selector outside a query statement', (done) ->
				compile done, '''
				 
					source http://processor.p23r.de/rule0815/source
					result {
						$
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '{ $__source }'

			it 'should select elements using the source selector', (done) ->
				compileQuery done, '''
				 
					source http://processor.p23r.de/rule0815/source
					result {
						$
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<result>\n  <a>\n    <b c="0815"/>\n    <d>xyz</d>abc</a>\n</result>'
				, 'selector'

			it 'should accept source selector with a path outside a query statement', (done) ->
				compile done, '''
				 
					source http://processor.p23r.de/rule0815/source
					result {
						$.a.b
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '{ $__source/*:a/*:b }'

			it 'should select an element using a path with the source selector', (done) ->
				compileQuery done, '''
				 
					source http://processor.p23r.de/rule0815/source
					result {
						$.a.b
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<result>\n  <b c="0815"/>\n</result>'
				, 'selector'

			it 'should accept source selector accessing the values of a node outside a query statement', (done) ->
				compile done, '''
				 
					source http://processor.p23r.de/rule0815/source
					result {
						$.a@
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '{ fn:data($__source/*:a/text()) }'

			it 'should select text content using a path with the source selector', (done) ->
				compileQuery done, '''
				 
					source http://processor.p23r.de/rule0815/source
					result {
						$.a@
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<result>abc</result>'
				, 'selector'

			it 'should accept source selector accessing all elements outside a query statement', (done) ->
				compile done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						$.a.*
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '{ $__source/*:a/* }'

			it 'should select elements using a path with the source selector', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						$.a.*
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<result>\n  <b c="0815"/>\n  <d>xyz</d>\n</result>'
				, 'selector'

			it 'should accept source selector with a path and an attribute outside a query statement', (done) ->
				compile done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						$.a.b@c
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '{ fn:data($__source/*:a/*:b/@*:c) }'

			it 'should select an attribute using a path with the source selector', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						$.a.b@c
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<result>0815</result>'
				, 'selector'

		describe 'P23R Selection @ processor for variables', ->

			it 'should reject undefined variables', (done) ->
				compile done, '''
				 
					result {
						ufo
					}
				''', (ok, result) ->
					expect(ok).toBe false
