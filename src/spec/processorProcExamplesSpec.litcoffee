
## P23R Selection Compiler - real world examples

Author: Jan Gottschick

To test the P23R selection compiler for the processor ...

Importing the Jasmine test framework addons to describe the specifications by
examples.

		require 'jasmine-matchers'
		require 'jasmine-given'

		specUtilProcessor = require './specUtilProcessor'
		compile = specUtilProcessor.compile
		compileQuery = specUtilProcessor.compileQuery
		noSpaces = specUtilProcessor.noSpaces
		query = require('./basexUtil').query
		setup = require('./basexUtil').setup

And the tests...

		describe 'P23R Selection @ processor example for the P23R handbook', ->

			beforeEach (done) ->
				setup ->
					done()
				, 'examplesSource', '''<?xml version="1.0" encoding="UTF-8"?>
					<enterpriseCore xmlns="http://leitstelle.p23r.de/NS/P23R/ruleGroups/Starter/enterpriseCore1-0">
							 <profile>
									 <company>
											 <name>Muster AG</name>
									 </company>
									 <headquarters>
											 <addresses>
												 <street>Hubertusallee</street>
												 <housenumber>8</housenumber>
												 <zipCode>12345</zipCode>
												 <city>Berlin</city>
												 <country>Germany</country>
												 <validFrom>2014-01-01</validFrom>
												 <lastModified>2013-11-01</lastModified>
											 </addresses>
											 <addresses>
													 <street>Lindenstraße</street>
													 <housenumber>16</housenumber>
													 <zipCode>12345</zipCode>
													 <city>Berlin</city>
													 <country>Germany</country>
													 <validFrom>2002-10-02</validFrom>
													 <lastModified>2002-10-02</lastModified>
											 </addresses>
											 <addresses>
													 <street>Lindenstraße</street>
													 <housenumber>1</housenumber>
													 <zipCode>12345</zipCode>
													 <city>Berlin</city>
													 <country>Germany</country>
													 <validFrom>1998-01-01</validFrom>
													 <lastModified>1998-01-01</lastModified>
											 </addresses>
									 </headquarters>
							 </profile>
					</enterpriseCore>'''
				setup ->
					done()
				, 'examplesProfile', '''
				'''

			it 'should generate correct code for the source connector', (done) ->
				compileQuery done, '''
				# Selection.dats

				Zunächst werden die Antragstellerdaten dem Modell Applicant zugeordnet.

					source http://leitstelle.p23r.de/NS/P23R/ruleGroups/Starter/enterpriseCore1-0
					selection {
						@xmlns { "http://leitstelle.p23r.de/NS/P23R/ruleGroups/Starter/Selection1-0" }
						[

				Und dann ein Profile erstellt

							for each applicant in $.enterpriseCore.profile
							take 1
							return profile {
								@name { applicant.company.name }
								[
									for each location in applicant.headquarters.addresses
									take -2
									return addresses {
										@validFrom { location.validFrom }
										@lastModified { location.lastModified }
										@houseNumber { location.housenumber }
										@street { location.street }
										@city { location.city }
										@zipCode { location.zipCode }
										@country { location.country }
									}
								]
							}
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(noSpaces(result)).toContain '<profilename="MusterAG"><addressesvalidFrom="2002-10-02"lastModified="2002-10-02"houseNumber="16"street="Lindenstraße"city="Berlin"zipCode="12345"country="Germany"/><addressesvalidFrom="1998-01-01"lastModified="1998-01-01"houseNumber="1"street="Lindenstraße"city="Berlin"zipCode="12345"country="Germany"/></profile>'
				, 'examples'
