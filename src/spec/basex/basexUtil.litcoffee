
Test function to execute xquery scripts with a local basex server.

Author: Jan Gottschick
License: BSD

    basexUtil = exports? and exports or @basexUtil = {}

    basex  = require "basex"
    basex.debug_mode = false

And the test function

    basexUtil.execute = (done, cmd, tests) ->

Initialze the connection to the basex server

      client = new basex.Session "127.0.0.1", 1984, "p23r", "aJ7k&dP$"

And execute the command

      client.execute cmd, tests
      client.close ->
        done()


    basexUtil.query = (done, expr, tests) ->

Initialze the connection to the basex server

      client = new basex.Session "127.0.0.1", 1984, "p23r", "aJ7k&dP$"

And execute the command

      client.execute 'xquery ' + expr, tests
      client.close ->
        done()

    basexUtil.setup = (done, name, data) ->

Prepare an example database

      client = new basex.Session "127.0.0.1", 1984, "p23r", "aJ7k&dP$"
      client.execute "drop database " + name, ->
        client.execute "create database " + name + " " + data, ->
          client.close ->
            done()
