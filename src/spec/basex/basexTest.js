/*
 * This example shows how database commands can be executed.
 */
var basex  = require("basex");
var client = new basex.Session("127.0.0.1", 1984, "p23r", "aJ7k&dP$");
basex.debug_mode = false;
/**
 * Description
 * @method print
 * @param {} err
 * @param {} reply
 * @return
 */
function print(err, reply) {
	if (err) {
		console.log("Error: " + err);
	} else {
    console.log("Got: " + reply.result)
		var t2=new Date();
		console.log("Execution completed in ",t2-t0," milliseconds.");
	}
};
var t0=new Date();
client.execute("info",print);
client.execute('xquery declare namespace p23r = "http://leitstelle.p23r.de/NS/p23r/processor/functions";\ndeclare variable $__ns__profile := "http://leitstelle.p23r.de/NS/p23r/nrl/notificationProfile";\ndeclare variable $__profile := doc($__ns__profile);\ndeclare variable $__ns__source := "http://processor.p23r.de/rule0815/source";\ndeclare variable $__source := doc("selector");\n<result>\n{ $__source/*:a/*:b }\n</result>', print)
client.close(function(){
	var t2=new Date();
	console.log("Closed in ",t2-t0," milliseconds.");
});
var t1=new Date();
// not a true time because basex commands not yet done.
console.log("Commands send in ",t1-t0," milliseconds.");
