## P23R Selection Compiler - basic basex tests

Author: Jan Gottschick

To test the P23R selection compiler the resulting XQuery code must be tested.
A _basex_ server is used to execute the resulting xquery scripts. basex is a
XQuery database with an integrated XQuery engine.

Importing the Jasmine test framework addons to describe the specifications by
examples.

    require 'jasmine-matchers'
    require 'jasmine-given'
    execute = require('./basexUtil').execute

And the tests...

    xdescribe 'the basex XQuery service', ->

      it 'should be available', (done) ->
        execute done, 'info', (err, reply) ->
          expect(reply.ok).toBe true

      it 'should run a correct script', (done) ->
        execute done, 'xquery 1 to 5', (err, reply) ->
          expect(reply.result).toBe '1 2 3 4 5'

      it 'should signal an incorrect script', (done) ->
        execute done, 'xry 1 to 5', (err, reply) ->
          expect(reply.ok).toBe false
