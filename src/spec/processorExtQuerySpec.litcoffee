
## P23R Selection Compiler - basic selection tests with external queries

Author: Jan Gottschick

To test the P23R selection compiler for the processor ...

Importing the Jasmine test framework addons to describe the specifications by
examples.

		require 'jasmine-matchers'
		require 'jasmine-given'

		specUtilProcessor = require './specUtilProcessor'
		compile = specUtilProcessor.compile
		compileQuery = specUtilProcessor.compileQuery
		query = require('./basexUtil').query
		setup = require('./basexUtil').setup

And the tests...

		describe 'P23R Selection @ processor for nested queries using chaining', ->

			beforeEach (done) ->
				setup ->
					done()
				, 'queryExtSource', '''
					<a>
						<d>qrst</d>
						<b c="0815"/>
						<d>xyz</d>
						<d>uvw</d>
						abc
					</a>
				'''
				setup ->
					done()
				, 'queryExtProfile', '''
					<profile>
						<enterprise name="apple"/>
						<enterprise name="amazon"/>
						<enterprise name="google"/>
					</profile>
				'''

			it 'should request data from a source connector', (done) ->
				compile done, '''
				 
					model X = http://examples.p23r.de/NS/X
					result {
						[
							for x in X
							return x
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toMatch /\{p23r:selectSourceData\( \$__ns_X, '\s*model X = http:\/\/examples.p23r.de\/NS\/X\s*\[\s*foreach x in X\s*return x\s*\]\s*'\)\}/

			it 'should request a xml document with a namespace', (done) ->
				compile done, '''
				 
					model X = http://examples.p23r.de/NS/X
					result {
						[
							for x in X
							return tag {
								123
								"hugo"
								@xmlns { "http://leitstelle.p23r.de/NS/Selection1-0" }
							}
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '@xmlns { "http://leitstelle.p23r.de/NS/Selection1-0" }'
