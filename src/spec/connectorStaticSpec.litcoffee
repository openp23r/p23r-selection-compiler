
## P23R Selection Compiler - basic selection tests with static data and comments

Author: Jan Gottschick

To test the P23R selection compiler for the connector ...

Importing the Jasmine test framework addons to describe the specifications by
examples.

		require 'jasmine-matchers'
		require 'jasmine-given'

		specUtilConnector = require './specUtilConnector'
		compile = specUtilConnector.compile
		compileQuery = specUtilConnector.compileQuery
		noSpaces = specUtilConnector.noSpaces
		query = require('./basexUtil').query

And the tests...

		describe 'the basex XQuery service @ connector', ->

			it 'should be available', (done) ->
				query done, '1 to 5', (err, reply) ->
					expect(reply.ok).toBe true


		describe 'comments in selection expressions @ connector', ->

			it 'should compile a pure single line comment to a single xquery comment', (done) ->
				compile done, 'This is a single comment', (ok, result) ->
					expect(ok).toBe true
					expect(result.trim()).toBe '(: This is a single comment :)'

			it 'should compile multiple comment block', (done) ->
				compile done, '''
				This is comment spanning
				multiple lines.

				This is a second comment block.
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result.trim()).toBe '(: This is comment spanning\nmultiple lines. :)\n(: This is a second comment block. :)'

			xit 'should compile a pure single line comment starting with a single letter to a single xquery comment', (done) ->
				compile done, 'I am a single comment', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '\n(: I am a single comment :)'

			it 'should accept initial comments', (done) ->
				compile done, '''
				 
				This is an initial comment surrounded by blank lines.
				 
					result {
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '(: This is an initial comment surrounded by blank lines. :)'

			it 'should accept final comments', (done) ->
				compile done, '''
					result {
					}
				This is an final comment.
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '(: This is an final comment. :)'

			xit 'should accept comments before elements', (done) ->
				compile done, '''
				 
					result {
				comments before elements without blank lines
						tt {}
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '(: comments before elements without blank lines :)'

			it 'should not accept comments before attributes', (done) ->
				compile done, '''
					result {
				comments before elements without blank lines
						@a {}
					}
				''', (ok, result) ->
					expect(ok).toBe false

			xit 'should accept comments before model definitions', (done) ->
				compile done, '''
				comments before model definitions without blank lines
					model Xyz=http://www.pp.de/NS/22/hs
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '(: comments before model definitions without blank lines :)'

			xit 'should accept comments before queries', (done) ->
				compile done, '''
					result {
				comments before queries without blank lines
						[]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '(: comments before queries without blank lines :)'

			xit 'should not accept comments before tag end', (done) ->
				compile done, '''
					result {
				comments before queries without blank lines
					}
				''', (ok, result) ->
					expect(ok).toBe false


		describe 'selection expressions evaluating static content @ connector', ->

			it 'should deliver a single element', (done) ->
				compileQuery done, '''
				 
					result {
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<result/>'

			it 'should deliver simply nested element', (done) ->
				compileQuery done, '''
				 
					result {
						a {}
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(noSpaces(result)).toBe '<result><a/></result>'

			it 'should deliver a single element', (done) ->
				compileQuery done, '''
				 
					result { "Hello World" }
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>\nHello World\n</result>'

			it 'should deliver resulting element values from string expressions', (done) ->
				compileQuery done,  '''
				 
					result {
						a { "abc" + "def" }
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '>abcdef<'

			it 'should not accept empty attributes', (done) ->
				compile done,  '''
				 
						result {
							@a {  }
						}
				''', (ok, result) ->
					expect(ok).toBe false

			it 'should deliver a single element and an integer attribute', (done) ->
				compileQuery done, '''
				 
					result {
						@count { 16 }
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<result count="16"/>'
					 
			it 'should deliver a single element and a floating attribute', (done) ->
				compileQuery done, '''
				 
					result {
						@count { 16.5e5 }
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<result count="16.5e5"/>'
					 
			it 'should deliver a single element and a time attribute', (done) ->
				compileQuery done, '''
				 
					result {
						@count { 16:03 }
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<result count="16:03"/>'

			it 'should deliver a single element and a datetime attribute', (done) ->
				compileQuery done, '''
				 
					result {
						@count { 2014-01-31t16:03:45 }
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<result count="2014-01-31t16:03:45"/>'

			it 'should deliver a single element and a date attribute', (done) ->
				compileQuery done, '''
				 
					result {
						@count { 2014-08-14 }
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<result count="2014-08-14"/>'

			it 'should deliver a single element and a string attribute', (done) ->
				compileQuery done, '''
				 
					result {
						@xmlns {"http://www.website.de/NS/schema.xsd"}
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<result xmlns="http://www.website.de/NS/schema.xsd"/>'

			it 'should deliver a single element and multiple attribute values', (done) ->
				compileQuery done, '''
				 
					result {
						@mixed { 2014-08-14 12 "Max Mustermann" }
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<result mixed="2014-08-14 12 Max Mustermann"/>'

			it 'should deliver a single element and multiple attributes', (done) ->
				compileQuery done, '''
				 
					result {
						@a1 { 2014-08-14 12 }
						@a2 { "Max Mustermann" }
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<result a1="2014-08-14 12" a2="Max Mustermann"/>'

			it 'should deliver nested elements', (done) ->
				compileQuery done, '''
				 
					outer {
						inner {
							"Max Mustermann"
						}
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<outer>\n  <inner>\nMax Mustermann\n</inner>\n</outer>'

			it 'should deliver nested elements and attributes', (done) ->
				compileQuery done, '''
				 
					outer {
						inner {
							@attr {
								2014-08-14
								12
								"Max Mustermann"
							}
						}
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<outer>\n  <inner attr="2014-08-14 12 Max Mustermann"/>\n</outer>'

			it 'should deliver deeper nested elements', (done) ->
				compileQuery done, '''
				 
					outer {
						inner {
							t {
								@attr {
									2014-08-14
									12
									"Max Mustermann"
								}
							}
						}
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<outer>\n  <inner>\n    <t attr="2014-08-14 12 Max Mustermann"/>\n  </inner>\n</outer>'

			it 'should deliver an element with multiple static values', (done) ->
				compileQuery done, '''
				 
					elem {
						2014-08-14
						12
						"Max Mustermann"
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<elem>\n2014-08-14\n12\nMax Mustermann\n</elem>'

			it 'should deliver an element with multiple static values even in a line', (done) ->
				compileQuery done, '''
				 
					result {
						@xmlns {"http://www.website.de/NS/schema.xsd"}
						1 2.1 (-456) 1.23e4 1.23e-4
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(noSpaces(result)).toContain '>12.11.23e41.23e-4-456</result>'
				, ''

			it 'should deliver resulting values from standard function calls', (done) ->
				compile done,  '''
				 
					result {
						@xmlns {"http://www.website.de/NS/schema.xsd"}
						P23R:protocolWrite(1,2,3)
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result.trim()).toContain '{p23r:protocolWrite( 1, 2, 3 )}'

			it 'should deliver resulting attribute values from string expressions', (done) ->
				compileQuery done,  '''
				 
					result {
						@a { "abc" + "def" }
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '"abcdef"'

			it 'should deliver resulting values from mixed expressions', (done) ->
				compileQuery done,  '''
				 
					result {
						@a { "abc" + 6 }
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '"abc6"'
