
Test functions to compile P23R selection expressions. Use the basex utilities to
verify the compiler results against a local running basex server.

		specUtil = exports? and exports or @specUtilConnector = {}
		Selection = require '../../lib/js/selectionConnector'
		_ = require 'underscore'

		query = require('./basexUtil').query

Compile the selection expression only to Xquery

		specUtil.compile = (__done, __expr, __test, __debug = false) ->
			console.log '===\n' + __expr.replace(/[\t]/g, "→ ").replace(/[\xA0]/g, "◻︎") + '\n===' if __debug
			try
				__code = Selection.parse(__expr)
			catch error
				console.log error.name + " at " + error.line + "," + error.column + ": " + error.message if __debug
				# console.trace()
				__test false, ''
				__done()
				return
			console.log '---\n' + __code.replace(/[\t]/g, "→ ") + '\n---' if __debug
			__test true, __code
			__done()

Compile the selection expression and execute the code with basex

		specUtil.compileQuery = (__done, __expr, __test, __db = '', __debug = false) ->
			console.log '===\n' + __expr.replace(/[\t]/g, "→ ").replace(/[\xA0]/g, "◻︎") + '\n===' if __debug
			try
				__code = Selection.parse(__expr)
			catch error
				console.log error.name + " at " + error.line + "," + error.column + ": " + error.message
				__test false, ''
				__done()
				return
			console.log '---\n' + __code.replace(/[\t]/g, "→ ") + '\n---' if __debug
			__code = __code.replace(/\$__parameter := doc\([^\).]*\)/, '$__parameter := doc("' + __db + 'ConnParameter")') if __db.length > 0
			console.log '-=-
			\n' + __code + '\n-=-' if __debug
			query __done, __code, (err, reply) ->
				console.log "BASEX ERROR: " + err if err
				__test(reply.ok, reply.result)

		specUtil.noSpaces = (s) ->
			return s.replace(/[\s\n\r]/g, '')
