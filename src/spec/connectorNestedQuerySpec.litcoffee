
## P23R Selection Compiler - basic selection tests with nested queries

Author: Jan Gottschick

To test the P23R selection compiler for the connector ...

Importing the Jasmine test framework addons to describe the specifications by
examples.

		require 'jasmine-matchers'
		require 'jasmine-given'

		specUtilConnector = require './specUtilConnector'
		compile = specUtilConnector.compile
		compileQuery = specUtilConnector.compileQuery
		query = require('./basexUtil').query
		setup = require('./basexUtil').setup

And the tests...

		describe 'P23R Selection @ connector for nested queries using chaining', ->

			it 'should return a range of values from a query', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for x in P23R:range(1,3)
							return x
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>1 2 3</result>'
				,'query'

			it 'should return a range of values from a chained query', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for x in P23R:range(1,3)
							for y in P23R:range(1,2)
							return x + y
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>2 3 3 4 4 5</result>'
				,'query'

			it 'should return a range of values from a nested query', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for x in P23R:range(1,3)
							return [
								for y in P23R:range(1,2)
								return x * y
							]
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>1 2 2 4 3 6</result>'
				,'query'

			it 'should sum up a range of values from a nested query', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for x in P23R:range(1,3)
							return 10 + [(
								for y in P23R:range(1,2)
								return x * y
							).Sum()]
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>13 16 19</result>'
				,'query'

			it 'should identify undefined variables outside a query (1)', (done) ->
				compile done, '''
				 
					result {
						x
						[
							for x in P23R:range(1,3)
							return x
						]
					}
				''', (ok, result) ->
					expect(ok).toBe false

			xit 'should identify undefined variables outside a query (2)', (done) ->
				compile done, '''
				 
					result {
						[
							for x in P23R:range(1,3)
							return x
						]
						x
					}
				''', (ok, result) ->
					expect(ok).toBe false
				,true
