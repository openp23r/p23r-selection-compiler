
## P23R Selection Compiler - basic selection tests with local queries

Author: Jan Gottschick

To test the P23R selection compiler for the connector ...

Importing the Jasmine test framework addons to describe the specifications by
examples.

		require 'jasmine-matchers'
		require 'jasmine-given'

		specUtilConnector = require './specUtilConnector'
		compile = specUtilConnector.compile
		compileQuery = specUtilConnector.compileQuery
		noSpaces = specUtilConnector.noSpaces
		query = require('./basexUtil').query
		setup = require('./basexUtil').setup

And the tests...

		describe 'P23R Selection @ connector for local queries using for and return', ->

			beforeEach (done) ->
				setup ->
					done()
				, 'queryConnParameter', '''
					<parameter>
						<a>
							<d>qrst</d>
							<b c="0815"/>
							<d>xyz</d>
							<d>uvw</d>
							abc
						</a>
					</parameter>
				'''

			it 'should accept a simple query statement which copies the parameter data', (done) ->
				compile done, '''
				 
					result {
						[
							for x
							return x
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(noSpaces(result)).toContain '<result>{for$xin$__parameterreturn$x}</result>'

			it 'should select the parameter in a query which copies the parameter data', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for x
							return x
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(noSpaces(result)).toBe '<result><parameter><a><d>qrst</d><bc="0815"/><d>xyz</d><d>uvw</d>abc</a></parameter></result>'
				, 'query'

			it 'should return an element from a query using a path', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for each x
							return x.parameter.a.b
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(noSpaces(result)).toBe '<result><bc="0815"/></result>'
				, 'query'

			it 'should return an attribute value from a query using a path', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for each x
							return x.parameter.a.b@c
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<result>0815</result>'
				, 'query'

			it 'should apply a method on the returning value from a query', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for each x
							return x.parameter.a@.ToUpper()
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<result>ABC</result>'
				, 'query'

			it 'should returning a constant value from a query', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for each x
							return "static"
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<result>static</result>'
				, 'query'

			it 'should apply a method on the query result', (done) ->
				compileQuery done, '''
				 
					result {
						[
							(
								for each x
								for y in x.parameter.a.d
								return "2"
							).Count()
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<result>3</result>'
				, 'query'

			it 'should apply a query inside an attribute', (done) ->
				compileQuery done, '''
				 
					result {
						@c {
							[
								for each x
								return x.parameter.a.b@c
							]
						}
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<result c="0815"/>'
				, 'query'

			it 'should apply a function call inside an attribute', (done) ->
				compileQuery done, '''
				 
					result {
						@c { range(1,3) }
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<result c="1 2 3"/>'
				, 'query'


		describe 'P23R Selection @ connector for local queries including skip and take', ->

			beforeEach (done) ->
				setup ->
					done()
				, 'queryConnParameter', '''
					<parameter>
						<a>
							<d>qrst</d>
							<b c="0815"/>
							<d>xyz</d>
							<d>uvw</d>
							abc
						</a>
					</parameter>
				'''

			it 'should skip a number of results in a query', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for x
							for y in x.parameter.a.d
							skip 1
							return y
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(noSpaces(result)).toContain '<result><d>xyz</d><d>uvw</d></result>'
				, 'query'

			it 'should deliver a limited number of results in a query', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for x
							for y in x.parameter.a.d
							take 1
							return y
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(noSpaces(result)).toContain '<result><d>qrst</d></result>'
				, 'query'

			it 'should deliver a range of results in a query', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for x
							for y in x.parameter.a.d
							skip 1
							take 1
							return y
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(noSpaces(result)).toContain '<result><d>xyz</d></result>'
				, 'query'

			it 'should deliver a negative take of results in a query', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for x in P23R:range(1,4)
							take -2
							return x
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>3 4</result>'
				, 'query'

			it 'should deliver a negative skip of results in a query', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for x in P23R:range(1,5)
							skip -2
							return x
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>1 2 3</result>'
				, 'query'

		describe 'P23R Selection @ connector for local queries including where', ->

			beforeEach (done) ->
				setup ->
					done()
				, 'queryConnParameter', '''
					<parameter>
						<a>
							<d>qrst</d>
							<b c="0815"/>
							<d>xyz</d>
							<d>uvw</d>
							<e>2</e>
							<e>5.5</e>
							abc
						</a>
					</parameter>
				'''

			it 'should compare a string value', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for x
							for y in x.parameter.a.d
							where y@ == "xyz"
							return y
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(noSpaces(result)).toContain '<result><d>xyz</d></result>'
				, 'query'

			it 'should use or condition operator', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for x
							for y in x.parameter.a.d
							where y@ == "xyz" || y@ == "uvw"
							return y
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(noSpaces(result)).toContain '<result><d>xyz</d><d>uvw</d></result>'
				, 'query'

			it 'should use not operator', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for x
							for y in x.parameter.a.d
							where ! y@ == "xyz"
							return y
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(noSpaces(result)).toContain '<result><d>qrst</d><d>uvw</d></result>'
				, 'query'

			it 'should use regex operator', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for x
							for y in x.parameter.a.d
							where y@ ~ /[qx]/
							return y
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(noSpaces(result)).toContain '<result><d>qrst</d><d>xyz</d></result>'
				, 'query'


		describe 'P23R Selection @ connector for local queries including let', ->

			beforeEach (done) ->
				setup ->
					done()
				, 'queryConnParameter', '''
					<parameter>
						<a>
							<d>qrst</d>
							<b c="0815"/>
							<d>xyz</d>
							<d>uvw</d>
							<e>2</e>
							<e>5.5</e>
							abc
						</a>
					</parameter>
				'''

			it 'should use single let', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for x
							for y in x.parameter.a.d
							let z = y@
							return z
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>qrst xyz uvw</result>'
				, 'query'

			it 'should use single let with string concatination', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for x
							for y in x.parameter.a.d
							let z = y@ + y@
							return z
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>qrstqrst xyzxyz uvwuvw</result>'
				, 'query'

			it 'should use single let with number addition', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for x
							for y in x.parameter.a.e
							let z = y@ + y@
							return z
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>4 11</result>'
				, 'query'

			it 'should use single let with string remove', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for x
							for y in x.parameter.a.d
							let z = y@ - "rst"
							return z
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>q xyz uvw</result>'
				, 'query'

			it 'should use single let with number subtraction', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for x
							for y in x.parameter.a.e
							let z = y@ - 1
							return z
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>1 4.5</result>'
				, 'query'

			it 'should use single let with string multiplication (1)', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for x
							for y in x.parameter.a.d
							let z = 3 * y@
							return z
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>qrstqrstqrst xyzxyzxyz uvwuvwuvw</result>'
				, 'query'

			it 'should use single let with string multiplication (2)', (done) ->
				compileQuery done, '''
				 
					result {
						[(
							for x
							for y in x.parameter.a.d
							let z = y@ * 3
							return z
						).Join()]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>qrstqrstqrstxyzxyzxyzuvwuvwuvw</result>'
				, 'query'

			it 'should use single let with number multiplication', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for x
							for y in x.parameter.a.e
							let z = 3 * y@
							return z
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>6 16.5</result>'
				, 'query'

			it 'should use single let with number division', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for x
							for y in x.parameter.a.e
							let z = y@ / 2
							return z
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(
						result).toContain '<result>1 2.75</result>'
				, 'query'

			it 'should use single let with enclosed expression', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for x
							for y in x.parameter.a.e
							let z = (y@ * 2) + 3
							return z
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>7 14</result>'
				, 'query'

		describe 'P23R Selection @ connector for local queries including orderby', ->

			it 'should order a range of values descending', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for y in P23R:range(1,3)
							orderby y descending
							return y
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>3 2 1</result>'
				, 'query'

		describe 'P23R Selection @ connector for local queries including distinct', ->

			beforeEach (done) ->
				setup ->
					done()
				, 'queryConnParameter', '''
				<in>
					<a>3</a>
					<a>4</a>
					<a>3</a>
					<b><c>1</c>5</b>
					<b>3</b>
					<b><c>1</c>5</b>
				</in>
				'''

			it 'should return only distinct values from values in outer selections', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for x in P23R:range(1,3)
							distinct
							for y in P23R:range(1,2)
							return x + y
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>2 3 4 5</result>'
				, 'query'

			it 'should return only distinct values from values in inner selections', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for x in P23R:range(1,3)
							for y in P23R:range(1,2)
							distinct
							return x + y
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>2 3 3 4 4 5</result>'
				, 'query'

			it 'should return only distinct values from element sequences', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for x
							for y in x.in.*
							distinct
							return y
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(noSpaces(result)).toContain '<result><a>3</a><a>4</a><b><c>1</c>5</b><b>3</b></result>'
				, 'query'

			it 'should return function results from a query', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for x in range(3,3)
							return range(1,2)
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(noSpaces(result)).toContain '<result>12</result>'
				, 'query'

			it 'should not iterate using numbers', (done) ->
				compile done, '''
				 
					result {
						[
							for x in 1
							return x
						]
					}
				''', (ok, result) ->
					expect(ok).toBe false
