
## P23R Selection Compiler - basic selection tests with local queries

Author: Jan Gottschick

To test the P23R selection compiler for the processor ...

Importing the Jasmine test framework addons to describe the specifications by
examples.

		require 'jasmine-matchers'
		require 'jasmine-given'

		specUtilProcessor = require './specUtilProcessor'
		compile = specUtilProcessor.compile
		compileQuery = specUtilProcessor.compileQuery
		noSpaces = specUtilProcessor.noSpaces
		query = require('./basexUtil').query
		setup = require('./basexUtil').setup

And the tests...

		describe 'P23R Selection @ processor for local queries using for and return', ->

			beforeEach (done) ->
				setup ->
					done()
				, 'querySource', '''
					<a>
						<d>qrst</d>
						<b c="0815"/>
						<d>xyz</d>
						<d>uvw</d>
						abc
					</a>
				'''
				setup ->
					done()
				, 'queryProfile', '''
					<profile>
						<rule name="first"/>
					</profile>
				'''

			it 'should accept a simple query statement which copies the source data', (done) ->
				compile done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $
							return x
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>\n{for $x in $__source\nreturn $x}\n</result>'

			it 'should select the source in a query which copies the source data', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $
							return x
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<result>\n  <a>\n    <d>qrst</d>\n    <b c="0815"/>\n    <d>xyz</d>\n    <d>uvw</d>abc</a>\n</result>'
				, 'query'

			it 'should select the source including a path in a query', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.d
							return x
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<result>\n  <d>qrst</d>\n  <d>xyz</d>\n  <d>uvw</d>\n</result>'
				, 'query'

			it 'should select the source requesting the text content in a query', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.d@
							return x
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<result>qrst xyz uvw</result>'
				, 'query'

			it 'should select the source including an attribute in a query', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.b@c
							return x
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<result>0815</result>'
				, 'query'

			it 'should select the profile in a query which copies the profile data', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							for each x
							return x
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<result>\n  <profile>\n    <rule name="first"/>\n  </profile>\n</result>'
				, 'query'

			it 'should return an element from a query using a path', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							for each x
							return x.profile.rule
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<result>\n  <rule name="first"/>\n</result>'
				, 'query'

			it 'should return an attribute value from a query using a path', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							for each x
							return x.profile.rule@name
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<result>first</result>'
				, 'query'

			it 'should apply a method on the returning value from a query', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							for each x
							return x.profile.rule@name.ToUpper()
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<result>FIRST</result>'
				, 'query'

			it 'should returning a constant value from a query', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							for each x in $.a.d
							return "static"
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<result>static static static</result>'
				, 'query'

			it 'should apply a method on the query result', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							(
								for each x in $.a.d
								return "2"
							).Count()
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<result>3</result>'
				, 'query'

			it 'should apply a query inside an attribute', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						@c {
							[
								for each x in $.a.b@c
								return x
							]
						}
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<result c="0815"/>'
				, 'query'

			it 'should apply a function call inside an attribute', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						@c { range(1,3) }
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toBe '<result c="1 2 3"/>'
				, 'query'

		describe 'P23R Selection @ processor for local queries including skip and take', ->

			beforeEach (done) ->
				setup ->
					done()
				, 'querySource', '''
					<a>"/>
						<d>qrst</d>
						<b c="0815"/>
						<d>xyz</d>
						<d>uvw</d>
						abc
					</a>
				'''
				setup ->
					done()
				, 'queryProfile', '''
					<profile>
						<rule name="first"/>
					</profile>
				'''

			it 'should skip a number of results in a query', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.d
							skip 1
							return x
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>\n  <d>xyz</d>\n  <d>uvw</d>\n</result>'
				, 'query'

			it 'should deliver a limited number of results in a query', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.d
							take 1
							return x
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>\n  <d>qrst</d>\n</result>'
				, 'query'

			it 'should deliver a range of results in a query', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.d
							skip 1
							take 1
							return x
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>\n  <d>xyz</d>\n</result>'
				, 'query'

			it 'should deliver a negative take of results in a query', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in P23R:range(1,4)
							take -2
							return x
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>3 4</result>'
				, 'query'

			it 'should deliver a negative skip of results in a query', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in P23R:range(1,5)
							skip -2
							return x
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>1 2 3</result>'
				, 'query'

		describe 'P23R Selection @ processor for local queries including where', ->

			beforeEach (done) ->
				setup ->
					done()
				, 'querySource', '''
					<a>"/>
						<d>qrst</d>
						<b c="0815"/>
						<d>xyz</d>
						<d>uvw</d>
						<e>2</e>
						<e>5.5</e>
						abc
					</a>
				'''
				setup ->
					done()
				, 'queryProfile', '''
					<profile>
						<rule name="first"/>
					</profile>
				'''

			it 'should compare a string value', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.d
							where x@ == "xyz"
							return x
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>\n  <d>xyz</d>\n</result>'
				, 'query'

			it 'should use or condition operator', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.d
							where x@ == "xyz" || x@ == "uvw"
							return x
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>\n  <d>xyz</d>\n  <d>uvw</d>\n</result>'
				, 'query'

			it 'should use not operator', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.d
							where ! x@ == "xyz"
							return x
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>\n  <d>qrst</d>\n  <d>uvw</d>\n</result>'
				, 'query'

			it 'should use regex operator', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.d
							where x@ ~ /[qx]/
							return x
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>\n  <d>qrst</d>\n  <d>xyz</d>\n</result>'
				, 'query'


		describe 'P23R Selection @ processor for local queries including let', ->

			it 'should use single let', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.d
							let y = x@
							return y
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>qrst xyz uvw</result>'
				, 'query'

			it 'should use single let with string concatination', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.d
							let y = x@ + x@
							return y
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>qrstqrst xyzxyz uvwuvw</result>'
				, 'query'

			it 'should use single let with number addition', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.e
							let y = x@ + x@
							return y
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>4 11</result>'
				, 'query'

			it 'should use single let with string remove', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.d
							let y = x@ - "rst"
							return y
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>q xyz uvw</result>'
				, 'query'

			it 'should use single let with number subtraction', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.e
							let y = x@ - 1
							return y
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>1 4.5</result>'
				, 'query'

			it 'should use single let with string multiplication (1)', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.d
							let y = 3 * x@
							return y
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>qrstqrstqrst xyzxyzxyz uvwuvwuvw</result>'
				, 'query'

			it 'should use single let with string multiplication (2)', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[(
							for x in $.a.d
							let y = x@ * 3
							return y
						).Join()]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>qrstqrstqrstxyzxyzxyzuvwuvwuvw</result>'
				, 'query'

			it 'should use single let with number multiplication', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.e
							let y = 3 * x@
							return y
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>6 16.5</result>'
				, 'query'

			it 'should use single let with number division', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.e
							let y = x@ / 2
							return y
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(
						result).toContain '<result>1 2.75</result>'
				, 'query'

			it 'should use single let with enclosed expression', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.e
							let y = (x@ * 2) + 3
							return y
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>7 14</result>'
				, 'query'

		describe 'P23R Selection @ processor for local queries including orderby', ->

			it 'should order a range of values descending', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in P23R:range(1,3)
							orderby x descending
							return x
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>3 2 1</result>'
				, 'query'

		describe 'P23R Selection @ processor for local queries including distinct', ->

			beforeEach (done) ->
				setup ->
					done()
				, 'querySource', '''
				<in>
					<a>3</a>
					<a>4</a>
					<a>3</a>
					<b><c>1</c>5</b>
					<b>3</b>
					<b><c>1</c>5</b>
				</in>
				'''
				setup ->
					done()
				, 'queryProfile', '''
					<profile>
						<rule name="first"/>
					</profile>
				'''

			it 'should return only distinct values from values in outer selections', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for x in P23R:range(1,3)
							distinct
							for y in P23R:range(1,2)
							return x + y
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>2 3 4 5</result>'
				, 'query'

			it 'should return only distinct values from values in inner selections', (done) ->
				compileQuery done, '''
				 
					result {
						[
							for x in P23R:range(1,3)
							for y in P23R:range(1,2)
							distinct
							return x + y
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(result).toContain '<result>2 3 3 4 4 5</result>'
				, 'query'

			it 'should return only distinct values from element sequences', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.in.*
							distinct
							return x
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(noSpaces(result)).toContain '<result><a>3</a><a>4</a><b><c>1</c>5</b><b>3</b></result>'
				, 'query'

			it 'should return function results from a query', (done) ->
				compileQuery done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in range(3,3)
							return range(1,2)
						]
					}
				''', (ok, result) ->
					expect(ok).toBe true
					expect(noSpaces(result)).toContain '<result>12</result>'
				, 'query'

			it 'should not iterate using numbers', (done) ->
				compile done, '''
				 
					source http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in 1
							return x
						]
					}
				''', (ok, result) ->
					expect(ok).toBe false
