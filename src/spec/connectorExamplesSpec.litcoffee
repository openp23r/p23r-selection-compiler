
## P23R Selection Compiler - real world examples

Author: Jan Gottschick

To test the P23R selection compiler for the connector ...

Importing the Jasmine test framework addons to describe the specifications by
examples.

		require 'jasmine-matchers'
		require 'jasmine-given'

		specUtilConnector = require './specUtilConnector'
		compile = specUtilConnector.compile
		compileQuery = specUtilConnector.compileQuery
		noSpaces = specUtilConnector.noSpaces
		query = require('./basexUtil').query
		setup = require('./basexUtil').setup

And the tests...

		describe 'P23R Selection @ connector example for the P23R handbook', ->

			beforeEach (done) ->
				setup ->
					done()
				, 'examplesSource', '''<?xml version="1.0" encoding="UTF-8"?>
					<enterpriseCore xmlns="http://leitstelle.p23r.de/NS/P23R/ruleGroups/Starter/enterpriseCore1-0">
							 <profile>
									 <company>
											 <name>Muster AG</name>
									 </company>
									 <headquarters>
											 <addresses>
												 <street>Hubertusallee</street>
												 <housenumber>8</housenumber>
												 <zipCode>12345</zipCode>
												 <city>Berlin</city>
												 <country>Germany</country>
												 <validFrom>2014-01-01</validFrom>
												 <lastModified>2013-11-01</lastModified>
											 </addresses>
											 <addresses>
													 <street>Lindenstraße</street>
													 <housenumber>16</housenumber>
													 <zipCode>12345</zipCode>
													 <city>Berlin</city>
													 <country>Germany</country>
													 <validFrom>2002-10-02</validFrom>
													 <lastModified>2002-10-02</lastModified>
											 </addresses>
											 <addresses>
													 <street>Lindenstraße</street>
													 <housenumber>1</housenumber>
													 <zipCode>12345</zipCode>
													 <city>Berlin</city>
													 <country>Germany</country>
													 <validFrom>1998-01-01</validFrom>
													 <lastModified>1998-01-01</lastModified>
											 </addresses>
									 </headquarters>
							 </profile>
					</enterpriseCore>'''
				setup ->
					done()
				, 'examplesConnParameter', '''
				'''

			xit 'should return the least two addresses', (done) ->
				compileQuery done, '''
					model Applicant = http://leitstelle.p23r.de/NS/P23R/ruleGroups/Starter/enterpriseCore1-0

					[

				Die beiden letzten Adressen werden aus dem Unternehmsprofil gefiltert und kopiert.

						for applicant in Applicant.enterpriseCore.profile
						take 1
						return profile {
							@xmlns { "http://leitstelle.p23r.de/NS/P23R/ruleGroups/Starter/AddressChange/Selection1-0" }
							company { @name { applicant.company@name } }
							[
								for location in applicant.headquarters
								orderby location@validFrom
								take -2

								return addresses {
									@validFrom { location@validFrom }
									@lastModified { location@lastModified }
									@houseNumber { location.address@houseNumber }
									@street { location.address@street }
									@city { location.address@city }
									@zipCode { location.address@zipCode }
									@country { location.address@country }
								}
							]
						}
					]

				''', (ok, result) ->
					expect(ok).toBe true
					expect(noSpaces(result)).toContain '<profilename="MusterAG"><addressesvalidFrom="2002-10-02"lastModified="2002-10-02"houseNumber="16"street="Lindenstraße"city="Berlin"zipCode="12345"country="Germany"/><addressesvalidFrom="1998-01-01"lastModified="1998-01-01"houseNumber="1"street="Lindenstraße"city="Berlin"zipCode="12345"country="Germany"/></profile>'
				, 'examples'
